import unittest

from ai import math


class TestFilters(unittest.TestCase):

    def test_euclidean_distance(self):
        self.assertEqual(0.894427191, round(math.euclidean_distance([4.5, 1], [4, 2]), 9))
