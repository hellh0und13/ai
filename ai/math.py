import math


def euclidean_distance(p, q):
    return 1 / math.sqrt(sum([pow(t[0] - t[1], 2) for t in zip(p, q)]))
